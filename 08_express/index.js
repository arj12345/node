const express = require("express");
const path = require('path')
const fs = require('fs/promises')

let STUDENTS = require('./students.json')

const app = express();

// 配置静态资源
app.use(express.static(path.resolve(__dirname, './public')))
// 配置请求体
app.use(express.urlencoded())

// 配置 express 的模板引擎 为 ejs
app.set('view engine', 'ejs')

// 配置模板路径
app.set('views', path.resolve(__dirname, 'views'))

app.post('/updateStu', (req, res)=>{
    const { id, name, age, gender, address } = req.body

    const student = STUDENTS.find(item=> item.id == id)
    student.name = name
    student.age = age
    student.gender = gender
    student.address = address

    fs.writeFile(path.resolve(__dirname, 'students.json'), JSON.stringify(STUDENTS))
    .then(()=>{
        res.redirect('/students')
    })
    .catch(()=>{
        res.send({ code: 305 })
    })
})

// 修改跳转路由
app.get('/toUpdate', (req, res)=>{
    const id = +req.query.id
    const modfiyData = STUDENTS.filter(item=> item.id === id)

    res.render('update', ...modfiyData)
})


// 删除路由
app.get('/delete', (req, res)=>{
    const id = +req.query.id
    STUDENTS = STUDENTS.filter(item=> item.id !== id)
    fs.writeFile(path.resolve(__dirname, 'students.json'), JSON.stringify(STUDENTS))
    .then(()=>{
        res.redirect('/students')
    })
    .catch(()=>{
        res.send({ code: 305 })
    })
})

app.get('/students', (req, res)=>{
    // res.render(path.resolve(__dirname, './views/students')) // 可以直接写路径
    res.render('students', { students: STUDENTS })
})

app.post('/addStu', (req, res)=>{
    const id = STUDENTS.length > 0 ? STUDENTS.at(-1).id + 1 : 1;

    const stu = { 
        id,
        name: req.body.name,
        gender: req.body.gender,
        age: req.body.age,
        address: req.body.address,
    }

    STUDENTS.push(stu)

    // 将新的内容写到 json 文件
    fs.writeFile(path.resolve(__dirname, 'students.json'), JSON.stringify(STUDENTS))
    .then(()=>{
        // 写入成功, 重定向回 /students 路由
        res.redirect('/students')
    })
    .catch(()=>{
        // 写入错误捕获错误内容
        res.send({ code: 305 })
    })

    // res.send('添加成功')
    // res.render('students', { students: STUDENTS })
    
    
})

// 在所有路由的后面, 配置错误路由
app.use((req, res)=>{
    // 只要这个中间件, 执行说明上面所有路由都没有匹配
    res.status(404)
    res.send("<h1>在光速飞行中迷失了方向~~~~</h1>")
})

app.listen(3000, ()=>{
    console.log('server ok.');
})


//[{"id":1,"name":"huilu","gender":"女","age":16,"address":"西城区"},{"id":2,"name":"huiya","gender":"女","age":16,"address":"西城区"},{"id":3,"name":"老八","gender":"男","age":"18","address":"公共厕所"}]