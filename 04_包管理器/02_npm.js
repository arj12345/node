/* 

    package.json
        scripts:
            - 可以自定义一些命令
            - 定义以后可以直接 通过 npm 来执行这些命令
            - start 和 test 可以直接通过 npm start/npm test 执行
            - 其他自定义的命令 需要通过 npm run xxx 运行

    npm 镜像：
        - npm 仓库位于国外，不好使
        - 解决问题：
            配置镜像服务器: 
                网址：https://npmmirror.com/
                ① 在系统中安装 cnpm 
                    npm install -g cnpm --registry=https://registry.npmmirror.com

                ② 彻底修改 npm 仓库
                    npm set registry https://registry.npmmirror.com
                    - 还原仓库
                        npm config delete registry
*/