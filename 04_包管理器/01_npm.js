/* 
    package.json
        - package.json 是包的描述文件
        - node 中通过该文件对项目进行描述
        - 每一个 node 项目必须有 package.json

        name 项目名称（必须）：
            字母（必须全部小写）、数字、下划线
        
        version 项目版本（必须）：
            X.Y.Z
            X: 代表大版本更新
            Y: 代表增加新的功能
            Z: 代表增打的补丁
        
        命令：
            npm init 初始化项目, 创建 package.json文件（需要回答问题）
            npm init -y 初始化项目, 创建 package.json文件（所有值都采用默认值）
            npm install 包名，指定包名下载到当前项目
                install 时发生了什么？
                    ① 将包下载当前项目的 node_modules 目录下
                    ② 会在 package.json 的 dependencies 属性中添加新的值
                        "dependencies": {
                            "lodash": "^4.17.21"
                        }
                    
                    ③ 自动添加 package-lock.json 文件
                        帮助加速 npm 下载, 不用动它
            
            npm install 自动安装所有依赖

            npm install 包名 -g 全局安装
                - 全局安装是将包安装到计算机中
                - 全局安装通常是一些工具

            npm uninstall 包名，卸载
*/

/* 
    引入 npm 下载的包时，不需要书写路径，直接写包名即可
*/
