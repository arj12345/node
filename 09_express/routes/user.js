const express = require('express')

const userRouter = express.Router()


// 返回登录界面
userRouter.get('/loginPage', (req, res)=>{
    res.render('login')
})

userRouter.get('/getCookie', (req, res)=>{

    /*
        安装 express 中间件解析 cookie 
        1. 安装 cookie-parser
            yarn add cookie-parser
        2. 引入
            const cookieParser = require("cookie-parser")
        3. 挂载
            app.use(cookieParser())
    */

    res.cookie('username', 'admin')
    
    res.send("Cookie 已经发送")
})

userRouter.get('/hello', (req, res)=>{
    console.log(req.cookies);
    res.send("欢迎使用 user 路由")
})

// 处理用户登录
userRouter.post('/login', (req, res)=>{
    
    const { username, password } = req.body

    // 将用户名放入 cookie
    res.cookie("username", username)

    if(username === 'admin' && password === 'root') {
        res.redirect('/students/list')
    }
    else
    {
        res.redirect('/user/loginPage')
    }
})

module.exports = userRouter