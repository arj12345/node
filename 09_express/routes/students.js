const express = require('express')
const fs = require("fs/promises")
const path = require("path")

const studentsRouter = express.Router()

const studentsDataPath = "../data/students.json"

let STUDENTS = require(studentsDataPath)

studentsRouter.get('/hello', (req, res)=>{
    res.send("欢迎访问 students 路由")
})

// 显示 学生列表
studentsRouter.get('/list', (req, res)=>{
    if(req.cookies.username) {
        res.render('students', { students: STUDENTS })
    }
    else
    {
        res.redirect('/')
    }
})

// 添加学生的路由
studentsRouter.post('/addStudent', (req, res, next)=>{
    const id = STUDENTS.at(-1) ? STUDENTS.at(-1).id+1 : 1
    
    const newStudentData = {
        id,
        name: req.body.name,
        age: req.body.age,
        gender: req.body.gender,
        address: req.body.address
    }

    STUDENTS.push(newStudentData)
    
    // 路由放行, 交给最后的 中间件进行文件保存操作
    next()
})

// 删除学生操作路由
studentsRouter.get("/delete", (req, res, next)=>{
    const id = +req.query.id

    STUDENTS = STUDENTS.filter(item=> item.id !== id)

    // 路由放行, 交给最后的 中间件进行文件保存操作
    next()
})


// 学生修改界面返回
studentsRouter.get('/toUpdate', (req, res)=>{
    const id = +req.query.id
    const modfiyStudentData = STUDENTS.find(item=> item.id === id)
    
    res.render('update', modfiyStudentData)
})

// 学生数据修改路由
studentsRouter.post('/update', (req, res, next)=>{

    const data = req.body
    const id = +data.id

    const oldData = STUDENTS.find(item=> item.id === id)

    oldData.name = data.name
    oldData.age = data.age
    oldData.gender = data.gender
    oldData.address = data.address

    // 路由放行, 交给最后的 中间件进行文件保存操作
    next()
})



// 处理保存数据的 路由
studentsRouter.use((req, res)=>{
    fs.writeFile(path.resolve(__dirname, studentsDataPath), JSON.stringify(STUDENTS))
    .then(()=>{
        res.redirect("/students/list")
    })
    .catch(()=>{
        res.send({ code: 305 })
    })
})

// Commonjs 模块化导出
module.exports = studentsRouter