const express = require("express");
const path = require('path')
const StudentsRouter = require("./routes/students") 
const UserRouter = require("./routes/user")
const cookieParser = require('cookie-parser')

const app = express();

app.listen(3000, ()=>{
    console.log('server ok.');
})

// 挂载 cookie-parser 中间件
app.use(cookieParser())

// 配置静态资源
// app.use(express.static(path.resolve(__dirname, './public')))
// 配置请求体
app.use(express.urlencoded())

// 配置 express 的模板引擎 为 ejs
app.set('view engine', 'ejs')

// 配置模板路径
app.set('views', path.resolve(__dirname, 'views'))

// 挂载用户处理路由
app.use('/user', UserRouter)

app.get('/', (req, res)=>{
    res.redirect('/user/loginPage')    
})

app.use('/students', StudentsRouter)


app.use((req, res) => {
    res.status(404).send("<h1>在光速飞行中迷失了方向~~</h1>")
})



//[{"id":1,"name":"huilu","gender":"女","age":16,"address":"西城区"},{"id":2,"name":"huiya","gender":"女","age":16,"address":"西城区"},{"id":3,"name":"老八","gender":"男","age":"18","address":"公共厕所"}]