const express = require("express");
const path = require('path')

const STUDENTS = [
    {
        name: "huilu",
        age: 16
    },
    {
        name: "huiya",
        age: 14
    },
    {
        name: "AliSi",
        age: '???'
    },
    {
        name: 'Meilin',
        age: '???'
    }
]

let name = 'huilu'

// const bodyParser = require('body-parser')

const app = express();

// 配置静态资源
app.use(express.static(path.resolve(__dirname, './public')))
// 配置请求体
app.use(express.urlencoded())

app.get('/hello', (req, res)=>{
    res.send('hello')
})

/* 
    html 页面属于静态网页
    
    node 模板:
        长得像网页, 可以嵌入变量, 数据随变量改变而改变

    ejs 是 node 中的一款模板引擎
        使用:
            1. 安装 ejs
            2. 配置 express 的模板引擎
            3. 配置模板路径
                app.set('views', path.resolve(__dirname, 'views'))
            3. 渲染并返回 res.render('模板名')

            注意: 模板引擎需要被 express 渲染后才能使用

        可以将对象作为 render 的第二个参数, 在模板中可以访问到对象中的数据

        <%= %> 在 ejs 中会自动将字符串中的特殊字符进行转义, 为避免 xss 攻击

*/

// 配置 express 的模板引擎 为 ejs
app.set('view engine', 'ejs')

// 配置模板路径
app.set('views', path.resolve(__dirname, 'views'))

app.get('/students', (req, res)=>{
    // res.render(path.resolve(__dirname, './views/students')) // 可以直接写路径
    res.render('students', { name: name, students: STUDENTS })
})

app.post('/setName', (req, res)=>{
    const nameModfiy = req.body.name
    name = nameModfiy
    
    res.send("修改成功")
})

// 在所有路由的后面, 配置错误路由
app.use((req, res)=>{
    // 只要这个中间件, 执行说明上面所有路由都没有匹配
    res.status(404)
    res.send("<h1>在光速飞行中迷失了方向~~~~</h1>")
})

app.listen(3000, ()=>{
    console.log('server ok.');
})