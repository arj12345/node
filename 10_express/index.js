const express = require("express");
const path = require('path')
const app = express();

const cookieParser = require("cookie-parser");

const session = require('express-session')

// 引入 session-file-store
const FileStore = require('session-file-store')(session);

const StudentsRouter = require("./routes/students") 
// const UserRouter = require("./routes/user")

app.listen(3000, ()=>{
    console.log('server ok.');
})

app.use(cookieParser())

// 配置请求体
app.use(express.urlencoded())

// 配置 express 的模板引擎 为 ejs
app.set('view engine', 'ejs')

// 配置模板路径
app.set('views', path.resolve(__dirname, 'views'))

// 挂载 session 中间件
app.use(
    session({
        store: new FileStore({
            reapInterval: 10,
            // 指定 session 本地文件的路径
            path: path.resolve(__dirname, "./session"),

            // 使用 密钥对 本地的 session 文件进行加密
            secret: "MIYao",

            // 设置 session 有效时间（计算客户端向服务器发送 ID 的闲置时间），默认为 3600 秒
            // ttl: 10,

            // 默认情况下 fileStore 会每隔一个小时，清除一次 session对象, 以秒为单位
            // reapInterval: 10
        }),
        secret: "dazhaxie"
    })
)

/* 
    session 是服务器中的一个对象，这个对象用来存储 用户信息 
        每一个 session 都会有一个唯一的id，session创建后，
            id会以 cookie 的形式被发送给浏览器

        浏览器收到后，每次访问都会将 id 发回，服务器中就可以根据 id 找到对应的 session

    id（cookie）--> session 对象

    session 什么时候会失效？
        第一种 浏览器的 cookie 没了
        第二种 服务器中的 session 对象没了

    express-session 默认将 session 存储到内存中的，所以服务器一旦重启 session 会自动重置
        使用session时，通常对 session 进行持久化操作（写到文件或数据库中）

    session 存储：
        引入中间件 session-file-store

        ① yarn add session-file-store

        ② var FileStore = require('session-file-store')(session);

        ③ app.use(
            session({
            store: new FileStore({}),
            secret: "dazhaxie"
        })
)
*/

app.use('/students', StudentsRouter)

// 返回登录界面
app.get('/', (req, res)=>{
    res.render('login')
})

app.get('/hello',(req, res)=>{
    console.log(req.session.loginUser);
    res.send("读取 Session");
})


// 处理登出
app.get("/loginOut", (req, res)=>{
    req.session.destroy(()=>{
        res.redirect("/")
    })
})

// 处理用户登录
app.post('/login', (req, res)=>{
    
    const { username, password } = req.body
    
    if(username === 'admin' && password === 'root') {
        // 将用户名放入 session 

        // 这里仅仅只是将 loginUser 存储到了内存中，并没有立即存储到文件里
        req.session.loginUser = username

        // 为了让 session 可以立即存储可以调用 save 来手动存储
        req.session.save(()=>{
            res.redirect('/students/list')
        })
    }
    else
    {
        res.send("用户密码错误.")
    }
})


// 挂载用户处理路由
// app.use('/user', UserRouter)

app.use((req, res) => {
    res.status(404).send("<h1>在光速飞行中迷失了方向~~</h1>")
})