const express = require("express");
const path = require('path')
const StudentsRouter = require("./routes/students") 
const UserRouter = require("./routes/user")
const cookieParser = require('cookie-parser')

const app = express();

app.listen(3000, ()=>{
    console.log('server ok.');
})

// 挂载 cookie-parser 中间件
app.use(cookieParser())

// 配置静态资源
// app.use(express.static(path.resolve(__dirname, './public')))
// 配置请求体
app.use(express.urlencoded())

// 配置 express 的模板引擎 为 ejs
app.set('view engine', 'ejs')

// 配置模板路径
app.set('views', path.resolve(__dirname, 'views'))

app.get('/set', (req, res)=>{
    res.cookie('name', 'huilu', {
        // expires: new Date()
        maxAge: 1000 * 60 * 60 * 24 * 30 // 毫秒为单位, 设置 cookie 过期时间
    })
    res.send("Cookie 设置成功.")
})

// 删除 cookie 只能通过重新设置覆盖的形式完成
app.get('/deleate', (req, res)=>{
    // cookie 设置以后不可修改, 所以只能通过 重新设置的方式达到删除目的
    res.cookie('name', '', {
        maxAge: 0
    })

    res.send("Cookie 已删除.")
})

app.get('/get', (req, res)=>{
    console.log(req.cookies);
    res.send("Cookie 读取成功.")
})