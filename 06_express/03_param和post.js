const express = require('express')
const path = require('path')
const app = express()

app.listen(3000, ()=>{
    console.log('server starting.');    
})

app.use(express.static(path.resolve(__dirname, './public')))

// 引入解析请求体的中间件
app.use(express.urlencoded())

app.get("/login", (request, response)=>{
    const username = request.query.username
    const password = request.query.password
    let sendString = ""

    if(username == 'admin' && password == 'root') {
        sendString = '登录成功.'
    }
    else {
        sendString = '用户或密码错误'
    }

    console.log(`接收到的数据: ${ username } - ${ password }`);
    response.send(`<h1>${ sendString }</h1>`)
})

//get 请求发送的第二中方式
// /hello/:id 表示当前用户访问 /hello/xx 时触发
// 在路径中以冒号命名的部分我们称为 param, 在 get 请求它可以被解析为请求参数
// param 传参不会传递非常复杂的参数
app.get("/hello/:id", (request, response)=>{
    console.log(request.params);
    response.send(`<h1>这是 hello 路由</h1>`)
})


// post 路由
app.post("/login", (request, response)=>{
    // 通过 request.body 来获取 post 请求的参数（请求体的参数）
    // 默认情况下 express 不会子哦对那个解析请求体，需要通过中间件来为其增加功能
    const username = request.body.username
    const password = request.body.password
    let sendString = ""

    if(username == 'admin' && password == 'root') {
        sendString = '登录成功.'
    }
    else {
        sendString = '用户或密码错误'
    }

    console.log(`接收到的数据: ${ username } - ${ password }`);
    response.send(`<h1>${ sendString }</h1>`)
})