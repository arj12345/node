/* 
    我们希望有一种方式，可以自动监视代码的修改
        代码修改自动重启
    
    使用 nodemon
        1. 全局安装
            npm i nodemon global
            yarn global add nodemon
                - 通过 yarn 进行全局安装时，默认yarn的目录并不在环境变量中
                - 需要手动将目录添加到环境变量中
                - 启动
                    nodemon .. //运行 index.js
                    nodemon xxx // 运行指定的 js

        2. 在项目中安装
            npm i nodemon -D
            yarn add nodemon -D 设置开发依赖

            - 启动
                npx nodemon

        希望用户返回根目录时，可以给用户返回一个网页
        
        服务器中的代码，对于外部来说都是不可见的，
            所以我们写的html页面，浏览器无法直接访问
            如果希望浏览器可以访问，则需要将页面所在的目录设置为静态资源目录
            
        设置 static 中间件后，会自动去 public 目录寻找静态资源
*/

// 引入 express
const express = require("express")
const path = require("path")

// 获取服务器的实例
const app = express()


// 启动服务器
app.listen(3000, ()=>{
    console.log("server start done.");
}) // 启动服务器

// 设置静态资源的目录
app.use(express.static(path.resolve(__dirname, './public')))


// 配置路由
app.get("/", (request, response)=>{
    response.send(`
    <!doctype html>
    <html>
        <head>
            <meta charset='utf-8'>
            <title>一个时代的进步</title>
        </head>
        <body>
            <h1>日新月异</h1>
        </body>
    </html>    
    `)
})

app.get("/login", (request, response)=>{
    const username = request.query.username
    const password = request.query.password
    let sendString = ""

    if(username == 'admin' && password == 'root') {
        sendString = '登录成功.'
    }
    else {
        sendString = '用户或密码错误'
    }

    console.log(`接收到的数据: ${ username } - ${ password }`);
    response.send(`<h1>${ sendString }</h1>`)
})