/* 
    express 是 node 中的服务器软件
        通过 express 快速搭建 web 服务器

        - 使用：
            1. 创建并初始化项目
                yarn init -y

            2. 安装 express
                yarn add express
            
            3. 创建 index.js 并编写代码

        如果希望服务器正常访问，则需要为服务器设置路由，
            路由可以根据不同的请求方式和请求地址处理用户的请求

            app.METHOD(...)
                METHOD 可以是
                    - get
                    - post

            路由回调执行时会收到三个参数
                - request
                    用户请求信息，获取用户传递数据
                - response
                    服务器向客户端返回的响应信息
                - next
                    是一个函数，调用后可以调用后面的中间件，不能在响应处理后 next

        中间件：
            - 在 express 中我们使用 app.use 来定义一个中间件
                中间件的租用路由很像，用法很像
                但是中间件不区分请求的方式，只看路径
            
            - 与路由的区别
                1. 会匹配所以请求
                2. 路径设置父目录
*/

// 引入 express
const express = require("express")

// 获取服务器的实例
const app = express()


// 启动服务器
app.listen(3000, ()=>{
    console.log("server start done.");
}) // 启动服务器


app.use('/hello', (request, response, next)=>{
    console.log('收到请回复');
    // response.send("111")
    next()
})
app.use('/hello', (request, response, next)=>{
    console.log('收到请回复');
    // response.send("222")
    next()
})
app.use('/hello', (request, response)=>{
    console.log('收到请回复');
    response.send("333")
})

app.get("/hello", (request, response)=>{
    // console.log('来了老弟儿 ~');
    // 在路由中，应该干两件事情
    // 读取用户请求 （request）
    // console.log(request.url);
    // 根据响应返回响应（response）

    // 向客户端发送响应状态码
    // response.sendStatus(404) 

    // 设置响应状态码，但不发送
    response.status(200)

    // 设置并设置响应体
    response.send("<h1>my first Server</h1>")
})