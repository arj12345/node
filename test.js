

function sum(a, b, call) {
    // while(true) {} // 阻塞
    // for(let i = 0; i < 10000000000; i++) {
    // } 
    // return a + b

    setTimeout(()=>{
        call( a + b )
    }, 100);

}

// console.log("我是一个输出")

// const result = sum(123, 456)

// console.log(result);

// console.log('你忽略了我');


sum(1, 2, (res)=>{
    sum(res, 2, (res)=>{
        sum(res, 2, (res)=>{
            console.log(res);
        })
    })
})