// Promise 就是用于存储数据的对象
//  存取方式特殊，可以将异步结果存储到 Promise 里
//      对 Promise 进行链式调用时
//      后边的方法（then和catch）读取的上一步的执行结果
//          如果上一步的执行结果不是当前想要的结果， 则跳过当前的方法
//      

/*
    当 Promise 出现异常时，而整个调用链中没有出现 catch，则异常会向外抛出
*/


const promise = new Promise((resolve, reason)=>{
    // resolve("开开心心过好每一天")
    reason("长江流水, 花落两人")
})

promise
    .then(r => console.log("第一个 then", r))
    .catch(r => {
        return "我们平凡的每一天都是一个个的奇迹"
    })
    .then(r => {
        console.log(r)
    })
    .catch(r => {
        console.log("我将所有的错误都包揽了.");
    })
   
    



// const promise = new Promise((resolve, reject)=>{
//     resolve("开开心心每一天 emo~~~")
// })

// promise.then((result)=>{
//     console.log(result);
// }, (reason)=>{
//     console.log("emo 了~~~_", reason);
// }) 

// Promise 解决回调地狱
// function sum(a, b, call) {
//     setTimeout(()=>{
//         call(a+b)
//     }, 1000)
// }



/*
    promise 的 then、catch、finally
            - 都会返回一个新的 Promise，Promise 中存储回调函数的返回值
            - finally 的回调返回值不会存储到 Promise 中
*/

// const promise = new Promise((resolve, reject)=>{
//     resolve("我们以后的每一天都要好好的生活！！")
// })
// const promise01 = promise.then((result)=>{
//     console.log(result);
//     return "时间之后故事之后的故事"
// })
// const promise02 = promise01.then((result)=>{
//     console.log(result);
// })


// 完美解决回调地狱问题
// function sum(a, b) {
//     return new Promise((resolve, reject)=>{
//         resolve(a+b)
//     })
// }
// sum(10, 10)
// .then((res) => res + 10)
// .then((res)=> console.log(res))

