
/*
    通过 async 可以快速创建异步函数
        异步函数的返回值会自动封装到一个 Promise 中返回

    在 async 声明的异步函数中可以使用 await 关键字来调用异步函数

    Promise 解决了异步调用中函数的回调地狱问题
        虽然通过链式调用解决了回调地狱，但是链式调用太多之后也不好看

    当我们通过 await 去调用时，它会暂停代码的运行
        直到异步代码执行有结果时，才会将结果返回
        注意：await 只能用于 async 声明的异步函数中，或 es模块的顶级作用域中
        await 阻塞的只是异步函数内部的代码，不会影响外部代码
        通过 await 调用异步代码，需要通过 try-catch 来处理异常

        如果 async 声明的函数没有写 await，那么它里面就会依次执行
        
*/

// function fn() {
//     return Promise.resolve(10)
// }
// fn().then((r)=>{
//     console.log(r);
// })

// async function fn() {
//     return 10
// }
// fn().then((res)=>{
//     console.log(res);
// })

// function sum(a, b) {
//     return new Promise(resolve => {
//         setTimeout(()=>{
//             resolve(a+b)
//         },2000)
//     })
// }

// async function fn() {

// }

// async function fn() {
//     console.log(1);
//     /*
//         当我们使用 await 调用函数后，当前函数后边的所有代码
//             会当前函数执行完毕后，被放入到微任务队列中
//     */
//     await console.log(2);
//     console.log(4);
//     console.log(5);
// }

// fn();

// console.log(3);


// function fn() {
//     return new Promise((resolve) => {
//         console.log(1);
//         console.log(2);
//         resolve()
//     }).then(r => {
//         // 加了 await 可以理解就是多了一个 then
//         console.log(4);
//     })
// }

// fn()
// console.log(3);

// async function fun() {

//     // return "我是一条调皮的数据"

//     console.log(1);
//     await console.log(2);
//     setTimeout(()=>{
//         console.log(5);
//     }, 1000)
// }

// fun()
// setTimeout(()=>{
//     console.log(4);
// }, 0)
// console.log(3);




// async function fn() {
//     await console.log(12);
// }

// fn()

function gb() {

}
gb();

(async ()=>{
    await console.log(12);
})()