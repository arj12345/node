
/*
    异步调用必须通过回调来接收返回值
        当我们的逻辑复杂时，就会出现 “回调地狱”

    问题：
        异步必须通过回调函数来返回结果，回调函数一多就会很痛苦
*/

// 创建 Promise()
// 创建 Promise 时，构造函数中需要一个函数作为参数
// Promise 构造函数的回调，它会在创建 Promise 时调用，调用时会有两个参数传递进去
// const promise = new Promise((resolve, reject)=>{
    // reslove 和 reject 两个函数，通过这两个函数可以向 Promise 存储数据
    // relove 在执行正常时存储数据，reject 在执行错误时存储数据

    // resolve("你好")
    // reject("我错误了")

    // 通过函数来向 Promise 添加数据，好处是可以在异步调用时存储数据

    // setTimeout(()=> {
        // resolve("哈哈")
        // reject("我错误了")
    // }, 2000)


    // throw new Error("我错了")
// })


// promise.then((result)=>{
//     // resolve 存储数据时执行
//     console.log("1", result)
// },
// (reason)=>{
//     // reject 存储数据或是出现错误时执行
//     console.log("2", reason)
// })



// setTimeout(()=>{
//     console.log(promise);
// }, 3000)


// 从 Promise 中读取属性
// - 可以通过 Promise 的实例方法 then 来读取 Promise 中存储的数据
// - then 需要两个回调函数作为参数，回调函数来获取 Promise 中的数据
//      通过 resolve 存储的数据会调用第一个函数返回, 我们可以在第一个函数中编写处理数据的代码
//      通过 reject 存储的数据或者出现异常时，会调用第二个函数返回，可以在第二个函数中编写处理异常的代码


/*
    Promise 中维护了两个隐藏属性：
        PromiseResult
            - 用来存储数据
        PromiseState
            - 记录 Promise 的状态（三种状态）
                pending （进行中）没有任何数据存入
                fulfilled （完成）通过 resolve 存储数据时
                rejected （拒绝，出错了）出错了或通过 reject 存储数据时

            - state 只能修改一次，修改以后永远不会再变

        流程：
            当 Promise 创建时 PromiseState 初始值为 pending
                当通过 resolve 存储数据时 PromiseState 变为 fulfilled
                    PromiseResult 变为存储的数据

                当通过 reject 存储数据或出错时 PromiseState 变为 rejected
                    PromiseResult 变为存储的 数据 或 异常对象

            当我们通过 then 读取数据时，相当于为 Promise 设置了回调函数，
                如果 PromiseState 变为 fulfilled，则调用 then 的第一个回调函数来返回数据
                如果 PromiseState 变为 rejected，则调用 then 的第二个回调函数来返回数据
                
*/

const promise = new Promise((resolve, reject)=>{
    // resolve("我是一条美好的数据")
    // reject("我是一条失败的数据")
    setTimeout(()=>{
        resolve("我是一条美好的数据")
    }, 1000)
})
// const promise = Promise.resolve("你好")
console.log(promise)
// promise.then((result)=>{
//     console.log(result);
// },
// (reason)=>{
//     console.log(reason);
// })

// console.log("我很皮")


/*
    catch() 用法与 then 类似，但是需要一个回调函数作为参数
        - catch() 中的回调只会再 promise 被拒绝时调用
        - catch() 相当于 then(null, reason => {})
        - catch() 就是一个专门处理 Promise 异常的方法

    finally()
        - 无论时正常出现存储数据还是出现了异常，finally 总会执行
        - 但是 finally 的回调函数中不会接收到数据
        - finally 通常用来 编写 无论成功与否都要执行的代码
*/

// promise.catch((reason)=>{
//     console.log(111);
// })

// promise.finally(()=>{
//     console.log("永不熄灭的火焰");
// })