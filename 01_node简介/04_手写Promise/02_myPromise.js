class myPromise {
    #result
    #PROMISE_STATE = {
        PENDING: 'pending',
        FULFILLED: 'fulfilled',
        REJECTED: 'rejected'
    }
    #state = this.#PROMISE_STATE.PENDING
    #resolveCallback
    #rejectCallback
    constructor(func) {
        func(this.#resolve.bind(this), this.#reject.bind(this))
    }
    #resolve(value) {
        if(this.#state !== this.#PROMISE_STATE.PENDING) return
        this.#result = value
        this.#state = this.#PROMISE_STATE.FULFILLED
        this.#resolveCallback && this.#rejectCallback(this.#result)
    }
    #reject(reason) {
        if(this.#state !== this.#PROMISE_STATE.PENDING) return
        this.#result = reason
        this.#state = this.#PROMISE_STATE.REJECTED
        this.#rejectCallback && this.#rejectCallback(this.#result)
    }
    then(onFulfilled, onProjected) {
        onFulfilled = typeof onFulfilled == "function" ? onFulfilled : ()=>{}
        onProjected = typeof onProjected == "function" ? onProjected : ()=>{}
        if(this.#state === this.#PROMISE_STATE.PENDING) {
            this.#resolveCallback = onFulfilled
            this.#rejectCallback = onProjected
        }
        if(this.#state === this.#PROMISE_STATE.FULFILLED) {
            onFulfilled(this.#result)
        }
        if(this.#state === this.#PROMISE_STATE.REJECTED) {
            onProjected(this.#result)
        }
    }
}
console.log("一");
const promise = new myPromise((resolve, reject)=>{
    setTimeout(()=>{
        resolve("我是一条调皮的数据")
    }, 1000)
    // resolve("生活如此美好")
    // reject("我是条调皮的数据")
})
promise.then(
    r => console.log('成功' + r)
)

console.log("二");
