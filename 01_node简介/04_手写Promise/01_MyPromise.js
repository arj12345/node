// 面向对象 JS 新特性

// 类 ——————————————————————————————————————

/*
    使用 Object 创建对象的问题：
        1. 无法区分出不同类型的对象
        2. 不方便批量创建对象
    
        JS 可以使用类（class）解决这个问题：
            1. 类是对象的模板，可以将属性和方法直接定义在类中
                定义后可以直接通过 类创建对象

            2. 通过一个类创建的对象，我们称为同类对象
                可以使用 instanceof 检查一个对象是否由这个类创建的
                如果某个对象是由某个类所创建，则我们称该对象是这个类的实例
        语法：
            class 类名 {}
            const 类名 = class {}

        
        类代码块内部默认是严格模式（"use strict"）
            在严格模式下所有指向 window 的 this 都是 undefined
                例如：
                    "use strict"
                    function call() {
                        console.log(this);
                    }
                    call()
            类的代码块是用来设置对象的属性的，不是什么代码都能写

        实例属性只能通过实例访问

        静态属性：
            使用 static 声明的属性，是静态属性（类属性）
                静态属性只能通过类去访问

        实例方法
            实例方法中的 this 是当前实例
        
        静态方法
            只能通过类来调用
            这里的 this 是当前的类

        在类中可以添加一个特殊的方法 constructor
            该方法为构造函数（构造方法）

    
    面向对象的特点：
        封装、继承、多态

        1. 封装
            - 对象就是一个用来存储不同属性的容器
            - 对象不仅负责存储数据，还要负责数据的安全
            - 直接添加到对象中的属性并不安全，可以随意修改
            - 如何确保数据安全：
                1. 私有化数据
                    实例属性使用 # 开头变为了私有属性，私有属性只能在类的内部访问
                2. 提供 setter 和 getter 方法来开放对数据的操作
                    - 属性设置私有使用 getter 和 setter 操作的好处
                        1. 可以控制属性的读写权限
                        2. 可以在方法中对属性的值进行验证

            - 封装主要用来保证数据的安全
            - 实现封装的方法：
                1. 属性私有化 加#
                2. 通过 getter 和 setter 方法来操作属性
                    get 属性() {
                        return this.#属性
                    }
                    set 属性(参数) {
                        this.#属性 = 参数
                    }

        2. 多态
            定义一个函数，这个函数将接收一个对象作为参数，它可以输出hello并打印对象的 name 属性

            在 JS 不会检查参数的类型，所以任何数据都可作为参数传递
            要调用某个函数，无需指定的类型，只要对象满足某些条件即可
            一个东西走路像鸭子，叫起来是鸭子，那么它就是鸭子
            多态为我们提供了灵活性

        3. 继承
            - 通过继承可以在不修改一个类的情况下来其进行扩展
            - OCP 开闭原则
                - 程序应该对修改关闭，对扩展开放

            在子类中，可以通过创建同名方法重写父类的方法
                重写构造函数时，构造函数的第一行代码必须为 super() 调用父类的构造函数
                在重写的方法中可以使用 super 来引用父类的方法
    
    对象中存储属性的区域实际上有两个
        1. 第一个位置时对象自身
            - 直接通过对象添加的属性
            - 在类中通过 x = y 的形式添加的属性

        2. 原型对象（prototype）
            - 对象中还有一些内容，会存储到它的对象里（原型对象）
            - 在对象中会有一个属性会存储对于原型的引用 __proto__

            - 原型对象也负责为对象存储属性
                当我们访问对象中的属性时，会优先访问自身的属性，
                对象自身不包括该属性时，才会去原型中寻找

            - 会添加到原型中的情况：
                1. 在类中通过 xxx(){} 方式添加的方法，位于原型中
                2. 主动向原型中添加的属性或方法
            
            - 获取对象原型的方法：
                对象.__proto__
                Object.getPrototypeOf(对象)
            
            - 原型对象中的数据：
                1. 对象的数据（属性、方法..）
                2. constructor（对象的构造函数）
            
            - 注意：原型对象也有原型，这样就构成了一条原型链，根据对象的复杂程度的不同长度也不同

            - 原型链：读取对象属性时，会先对从对象的自身开始寻找, 找到则应用
                    如果没有则去原型的原型去寻找直到 Object 对象， Object 对象没有原型此时返回 undefined
            
            - 作用域链，是寻找变量，找不到报错
            - 原型链，寻找属性，找不到，返回 undefined

            - 所有同类型对象他们的原型是同一个，同类型的原型链是一样的

            - 原型的作用：
                - 原型相当于公共的区域可以被所有实例访问到
                    可以将该类中的公共属性（方法）存储到原型中，创建一个属性可以被所有实例访问（节省性能开销）

            - JS 中的继承是完全依赖原型实现继承
                - 当我们继承时 子类 的原型就是类的实例
*/

const PROMISE_STATE = {
    PENDING: 'pending',
    FULFILLED: 'fulfilled',
    REJECTED: 'rejected'
}

class myPromise {
    #result
    #state = PROMISE_STATE.PENDING
    #callbacks = []
    constructor(excutor) {
        excutor(this.#resolve.bind(this), this.#reject.bind(this))
    }
    #resolve(value) {
        if(this.#state !== PROMISE_STATE.PENDING) return
        this.#result = value
        this.#state = PROMISE_STATE.FULFILLED
        queueMicrotask(()=>{
            // this.#callback && this.#callback(this.#result)
            this.#callbacks.forEach(call =>{
                call()
            })
        })
    }
    #reject(reason) { }
    then(onFufilled, onRejected) {
        return new myPromise((resolve, reject)=>{
            if(this.#state === PROMISE_STATE.PENDING) {
                // this.#callback = onFufilled
                this.#callbacks.push(()=>{
                    resolve(onFufilled(this.#result))
                })
            }
            if(this.#state === PROMISE_STATE.FULFILLED) {
                queueMicrotask((()=>{
                    resolve(onFufilled(this.#result))
                }))
            }
        })
    }
}

// const promise = new myPromise((resolve, reject) => {
//     setTimeout(()=>{
//         resolve("我是一条调皮的数据")
//     }, 1000)
// })

// promise.then((res) => {
//     console.log(res);
//     return "data: "+ res
// })
// .then((res)=>{
//     console.log(res);
// })


const promise = new Promise((resolve, reject)=>{
    throw new Error("你奈我何")
    // resolve("我是条调皮的数据")
})

promise.then((value)=>{
    console.log(value);
},(reason)=>{
    console.log(reason);
});

// const promise1 = promise.then((res)=>{
//     console.log(res);
//     return "Data: "+ res
// })

// promise1.then((res)=>{
//     console.log(res);
// })