/*
    Promise 的静态方法
        Promise.resolve() 创建一个立即完成的 Promise
        Promise.reject() 创建一个立即拒接的 Promise
        Promise.all([...]) 同时返回多个 Promise 的执行结果
            其中有一个报错，就会返回错误

        Promise.allSettled([...]) 同时返回多个 Promise 的执行结果（无论成功或失败）
            {status: 'fulfilled', value: 4}            成功
            {status: 'rejected', reason: '报个错'}      失败

        Promise.race([...]) 返回执行最快的 Promise（不考虑对错）

        Promise.any([...]) 返回执行最快的完成的 Promise
*/

function sum(a, b) {
    return new Promise(((resolve, resject) => {
        setTimeout(() => {
            resolve(a + b)
        }, 100)
    }))
}

// Promise.allSettled([
//     sum(1, 2),
//     sum(2, 2),
//     Promise.reject("报个错"),
//     sum(1, 3),
// ])
// .then((res)=>{
//     console.log(res);
// })

// Promise.race([
//     sum(1, 2),
//     sum(2, 2),
//     // Promise.resolve("你好"),
//     Promise.reject("你好"),
//     sum(3, 2),
// ])
// .then((res)=>{
//     console.log(res);
// })
// .catch(r=>console.log("错误"))

// Promise.any([
//     // sum(1, 2),
//     // sum(2, 2),
//     // Promise.resolve("你好"),
//     // Promise.reject("你好"),
//     // sum(3, 2),
//     // sum(10, 2),
//     // sum(3, 3),
//     Promise.reject("你好"),
//     Promise.reject("你好"),
//     Promise.reject("你好"),
// ])
// .then((res)=>{
//     console.log(res);
// })
// .catch(r=>console.log("错误", r))

