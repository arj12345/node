const express = require('express')

const userRouter = express.Router()

// 返回登录界面
userRouter.get('/loginPage', (req, res)=>{
    res.render('login')
})

// 处理用户登录
userRouter.post('/login', (req, res)=>{
    
    const { username, password } = req.body
    
    if(username === 'admin' && password === 'root') {
        // 将用户名放入 session
        req.session.username = username
        res.redirect('/students/list')
    }
    else
    {
        console.log("密码错误.");
        res.redirect('/user/loginPage')
    }

    
})

module.exports = userRouter