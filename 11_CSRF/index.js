const express = require("express");
const path = require('path')
const app = express();

const cookieParser = require("cookie-parser");

const session = require('express-session')

// 引入 session-file-store
const FileStore = require('session-file-store')(session);

const StudentsRouter = require("./routes/students") 
// const UserRouter = require("./routes/user")

app.listen(3000, ()=>{
    console.log('server ok.');
})

app.use(express.static(path.resolve(__dirname, 'public')))

app.use(cookieParser())

// 配置请求体
app.use(express.urlencoded())

// 配置 express 的模板引擎 为 ejs
app.set('view engine', 'ejs')

// 配置模板路径
app.set('views', path.resolve(__dirname, 'views'))

// 挂载 session 中间件
app.use(
    session({
        store: new FileStore({
            reapInterval: 10,
            // 指定 session 本地文件的路径
            path: path.resolve(__dirname, "./session"),

            // 使用 密钥对 本地的 session 文件进行加密
            secret: "MIYao",

            // 设置 session 有效时间（计算客户端向服务器发送 ID 的闲置时间），默认为 3600 秒
            // ttl: 10,

            // 默认情况下 fileStore 会每隔一个小时，清除一次 session对象, 以秒为单位
            // reapInterval: 10
        }),
        secret: "dazhaxie"
    })
)

/* 
    CSRF 攻击
        - 跨站请求伪造
        - 现在大部分的浏览器都不会在跨域的情况下自动发送 cookie
            这个设计时为了避免 CSRF 攻击

        - 如何解决？
            1. 使用 referer 头来检查请求的来源
            2. 使用验证码
            3. 尽量使用 post 请求（集合 token）

        - token（令牌）
            可以在创建表单时随机生成一个令牌
                然后将令牌存储到 session 中，并通过 模板 发送给用户，用户提交表单时，必须将 token 发回，才可以进行后续操作
                (可以使用 uuid 来生成 token)
            
*/

app.use('/students', StudentsRouter)

// 返回登录界面
app.get('/', (req, res)=>{
    res.render('login')
})

app.get('/hello',(req, res)=>{
    console.log(req.session.loginUser);
    res.send("读取 Session");
})


// 处理登出
app.get("/loginOut", (req, res)=>{
    req.session.destroy(()=>{
        res.redirect("/")
    })
})

// 处理用户登录
app.post('/login', (req, res)=>{
    
    const { username, password } = req.body
    
    if(username === 'admin' && password === 'root') {
        // 将用户名放入 session 

        // 这里仅仅只是将 loginUser 存储到了内存中，并没有立即存储到文件里
        req.session.loginUser = username

        // 为了让 session 可以立即存储可以调用 save 来手动存储
        req.session.save(()=>{
            res.redirect('/students/list')
        })
    }
    else
    {
        res.send("用户密码错误.")
    }
})


// 挂载用户处理路由
// app.use('/user', UserRouter)

app.use((req, res) => {
    res.status(404).send("<h1>在光速飞行中迷失了方向~~</h1>")
})