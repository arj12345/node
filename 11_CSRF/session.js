const express = require("express");
const session = require("express-session")

const app = express();

app.listen(3000, ()=>{
    console.log('server ok.');
})

// 挂载 session 中间件
app.use(session({
    secret: "MiYao" // 加密密钥, 随便设置
}))

app.get('/set', (req, res)=>{

    req.session.username = "huilu"
    
    res.send("查看 Session.")
})

app.get('/get', (req, res)=>{
    const username = req.session.username
    console.log(username);
    res.send("读取 session.")
})

app.use((req, res) => {
    res.status(404).send("<h1>在光速飞行中迷失了方向~~</h1>")
})
