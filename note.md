## nvm

nvm是用来管理 node 版本的工具

nvm

 - 命令

   - `nvm list` 显示已经安装的node 版本
   - `nvm install 版本` 安装指定版本的 node
   - 配置国内的镜像服务器

     ```bash
     nvm node_mirror https://npmmirror.com/mirrors/node/
     ```

node 和 JavaScript 有什么区别​

​	ECMAScript（node 有）、DOM、 BOM（node 没有）

​	node 保留了一些DOM和BOM好用的API如： setTimeout等



## 同步异步

进程和线程

	- 进程（厂房）：程序的运行环境
	- 线程（工人）：实际运行运算的东西

### 同步

 - 通常情况下代码都是自上向下一行一行执行
 - 前面代码不执行后面的代码也不会执行
 - 同步代码执行会出现阻塞情况
 - 一行代码执行慢会影响整个程序的执行

解决同步的问题：

 - java、python
   - 通过多线程解决
 - node.js
   - 通过异步方式来解决

### 异步

- 一段代码的执行不会阻碍其他的代码执行

- 异步的问题：

  - 异步的代码无法通过 return 来设置返回值

- 特点：

  1. 不会阻塞其他代码的执行
  2. 需要通过回调返回函数的结果

- 基于回调函数的异步带来的问题 

  ```javascript
  function sum(a, b, call) {
      setTimeout(()=>{
          call( a + b )
      }, 100);
  }
  
  // 回调地狱
  sum(1, 2, (res)=>{
      sum(res, 2, (res)=>{
          sum(res, 2, (res)=>{
              console.log(res);
          })
      })
  })
  ```

  1. 代码的可读性差
  2. 可调式性太差

- 解决问题：

  - 需要一个东西，可以代替回调函数来给我们返回结果
  - Promise 横空出世
    - Promise 是一个可以存储数据的对象

## Promise

	- Promise 可以帮助我们解决异步中的 __回调地狱__ 问题
	- Promise 存储数据的方式比较特殊，这种特殊方式使得 Promise 可以用来存储异步调的数据

#### 创建Promise

```javascript
const promise = new Promise((resolve, reject)=>{
    resolve("一个返回的数据")
    //reject("一个返回的数据")
})
```

- Promise 构造函数的 `回调`，它会在创建 `Promise 时调用`，调用时会有 `两个参数` 传递进去
- `reslove` 和 `reject` 是两个函数，通过这两个函数可以向 Promise 存储数据
  - `relove` 在执行正常时存储数据
  - `reject` 在执行错误时存储数据
- 通过`函数来向 Promise 添加数据`，好处是可以在异步调用时存储数据

#### 读取 Promise 数据

- 可以通过 Promise 的实例方法 then 来读取 Promise 中存储的数据

- `then` 需要两个`回调函数`作为参数，回调函数来获取 `Promise 中的数据`

  ```javascript
  promise.then((result)=>{
      // resolve 存储的数据调用
  },(reason)=>{
      // reject 存储的数据或者出现异常时调用
  })
  ```

  - 通过 resolve 存储的数据会调用第一个函数返回, 我们可以在第一个函数中编写处理数据的代码
  - 通过 reject 存储的数据或者出现异常时，会调用第二个函数返回，可以在第二个函数中编写处理异常的代码

#### Promise 中两个隐藏属性

```javascript
Promise {[[PromiseState]]: 'fulfilled', [[PromiseResult]]: '我是一条失败的数据', Symbol(async_id_symbol): 5, Symbol(trigger_async_id_symbol): 1}
```

- PromiseResult

  - 用来存储数据

- PromiseState

  - 记录 Promise 的状态（三种状态）

    ​	pending （进行中）没有任何数据存入

    ​	fulfilled （完成）通过 resolve 存储数据时

    ​	rejected （拒绝，出错了）出错了或通过 reject 存储数据时

  - state 只能修改一次，修改以后永不再变

#### Promise 流程

当 Promise 创建时 PromiseState 初始值为 pending

```javascript
[[PromiseState]]: 'pending', [[PromiseResult]]: undefined
```

- 当通过 resolve 存储数据时 PromiseState 变为 fulfilled

  - PromiseResult 变为存储的数据

    ```javascript
    [[PromiseState]]: 'fulfilled', [[PromiseResult]]: '我是一条成功的数据'
    ```

- 当通过 reject 存储数据或出错时 PromiseState 变为 rejected

  - PromiseResult 变为存储的 数据 或 异常对象

    ```javascript
    [[PromiseState]]: 'rejected', [[PromiseResult]]: '我是一条失败的数据'
    ```

- 当我们通过 then 读取数据时，相当于为 Promise 设置了回调函数
  - 如果 PromiseState 变为 fulfilled，则调用 then 的第一个回调函数来返回数据
  - 如果 PromiseState 变为 rejected，则调用 then 的第二个回调函数来返回数据

#### Promise 的 catch 与 finally

- `catch()` 用法与 then 类似，但是需要一个回调函数作为参数

     - catch() 中的回调只会再 promise 被`拒绝时调用`
             - catch() 相当于 `then(null, reason => {})`

     - catch() 就是一个`专门处理 Promise 异常`的方法

  ```javascript
  promise.catch((reason)=>{
  	// reject 存储数据时或出错时调用
  })
  ```

- `finally()`

  - 无论时正常出现存储数据还是出现了异常，`finally 总会执行`
     finally 的回调函数中不会接收到数据
  - finally 通常用来 编写 无论成功与否都要执行的代码

  ```javascript
  promise.finally(()=>{
  	console.log("永不熄灭的火焰");
  })
  ```

#### then、catch、finally 的返回值

promise 的 then、catch、finally 

- 都会返回一个新的 Promise，Promise 中存储回调函数的返回值
- finally 的回调返回值不会存储到 Promise 中

then 的链式调用

```javascript
const promise = new Promise((resolve, reject)=>{
    resolve("我们以后的每一天都要好好的生活！！")
})
.then((result)=>{
    console.log(result);
    return "时间之后故事之后的故事"
})
.then((result)=>{
    console.log(result);
})
```

