## HTTP 协议

- 网络基础

- 服务器

  - 一个服务器的主要功能：
    1. 可以接收到浏览器发送的请求报文
    2. 可以向浏览器返回响应报文

- 网络的服务器基于请求和响应的

  > `https://` 协议名

  > `baidu.com` 域名 
  >
  > 整个网络中存在无数个服务器，每一个服务器都有自己的唯一标识
  >
  > ​	这个标识为  `IP 地址` 但是，`IP地址` 记忆不方便
  >
  > ​	域名就相当于 `IP地址` 的别名

  > `/main/index.html` 网站资源路径
  >
  > ​	

  1. 当在浏览器中输入地址以后发生了什么

     1. `DNS解析` 获取网站的 `IP地址`

     2. 浏览器需要和服务器建立连接（`TCP/IP`）（三次握手）

     3. 向服务器发送请求（`http协议`）

     4. 服务器处理请求，并返回响应（`http协议`）

     5. 浏览器将响应的页面渲染

     6. 断开与服务器的连接（四次挥手）

        

  2. 客户端如何与服务器建立（断开）连接

  - 通过三次握手和四次挥手

    - 三次握手（建立连接）

      - 三次握手是客户端和服务器建立连接的过程

        1. 客户端向服务器发送连接请求  

           `SYN`

        2. 服务器收到请求，向客户端返回消息

           `SYN` `ACK`

        3. 客户端向服务器发送同意连接信息 

            `ACK`

      

    - 四次挥手（断开连接）

      1. 客户端向服务器发送请求，通知服务器数据发送完毕，请求断开连接

         `FIN`

      2. 服务器向客户端返回数据，表示已经接受请求

         `ACK`

      3. 服务器向客户端返回数据，数据接收完毕，可以断开

         `FIN` `ACK`

      4. 客户端向服务器返回确认信息

         `ACK`

- 请求和响应实际上就是一段数据只是这段是需要遵循一个特殊的格式
  - 这个特殊的格式由 `HTTP协议` 

## `TCP/IP` 协议族

 - `TCP/IP` 协议族中包含了一组协议

   这组协议规定了互联网中所有的通信细节

 - 网络通信的过程由四层组成

   - 应用层

     ​	软件层面，浏览器 服务器都属于应用层

   - 传输层

     ​	负责对数据进行拆分，把数据拆分为一个一个小包

   - 网络层

     ​	负责数据包，添加信息

   - 数据链路层

     ​	传输信息

 - HTTP协议就是在应用层的协议，用于规定客户端和服务器间通讯的报文格式

 - 报文？

   - 浏览器和服务器之间通信基于请求和响应的

     - 浏览器向服务器发送请求（request）
     - 服务器向浏览器返回响应（response）
     - 客户端向服务器`发送请求`相当于浏览器给服务器写信，服务器向浏览器`返回响应`，相当与服务器回信，这封信就叫做 `报文`
     - 而`HTTP协议`就是对这个报文的格`式进行规定`

   - 请求报文（request）

     - 客户端发送给服务器的报文称称为请求报文

     - 请求报文格式：

       - 请求首行

       - 请求头

       - 空行

       - 请求体

       > 请求首行
       >
       > `GET /05_http/01_http.html ?username=332075 HTTP/1.1`
       >
       > 
       >
       > 第一部分 GET 表示请求的方式，get 表示发送的是 get 请求
       >
       > 现在常用方式就是 `get` 和 `post` 请求
       >
       > - `get`请求主要用来向请求服务器资源
       >   - get 请求通过查询字符串将数据发送给服务器，由于字符串会在地址栏显示，所以它安全性较差
       >   - 同时由于 `url` 地址长度有限制，`get` 请求无法发送较大的数据
       > - `post` 主要用于向服务器提交请求
       >   - 在 chrome 中通过 payload（载荷）可以查看
       >   - post 请求通过请求体发送数据，无法在地址栏直接查看，安全性较好
       >   - 请求体的大小没有限制，可以发送任意大小
       >
       > 
       >
       > 第二部分 `/05_http/01_http.html` 请求资源的路径
       >
       > ?后面的内容叫查询字符串
       >
       > - 查询字符串是一个名值对结构，一个名子对应一个值
       >
       > - 使用 = 连接, 多个名值之间使用 & 分割 `username=3320753238&password=220`
       >
       > 
       >
       > 第三部分 `HTTP/1.1` 协议版本
       >
       > 

       > 请求头:
       >
       > - 请求头也是名值对结构，用来告诉服务器的信息
       >
       > - 每一个请求头都有它的作用
       >
       >   - `Accept` 浏览器可以接收的文件的类型
       >   - `Accept-Encoding` 浏览器允许的压缩编码
       >   - `Accept-Language` 浏览器可以接受的语言
       >   - `User-Agent` 用户代理，他是一段用来描述浏览器信息的字符串
       >
       >   
       >
       > `Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signedexchange;v=b3;q=0.7`
       >
       > `Accept-Encoding: gzip, deflate, br`
       >
       > `Accept-Language: zh-CN,zh;q=0.9`
       >
       > `Connection: keep-alive Host: 127.0.0.1:5500 `
       >
       > `Referer: http://127.0.0.1:5500/05_http%E5%8D%8F%E8%AE%AE/01_http.html `
       >
       > `Sec-Fetch-Dest: document `
       >
       > `Sec-Fetch-Mode: navigate`
       >
       > `Sec-Fetch-Site: same-origin `
       >
       > `Sec-Fetch-User: ?1 `
       >
       > `Upgrade-Insecure-Requests: 1 `
       >
       > `User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36 `
       >
       > `sec-ch-ua: "Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24" `
       >
       > `sec-ch-ua-mobile: ?0 sec-ch-ua-platform: "Windows"`

       > 空行：用来分割请求头与请求体

       > 请求体
       >
       > - post 请求通过请求体来发送数据

     - 响应报文：

       - 响应首行
       - 响应头
       - 空行
       - 响应体

       > 响应首行：`HTTP/1.1 200 OK`
       >
       > `HTTP/1.1` 协议版本
       >
       > `200` 响应状态码
       >
       >  `OK` 对响应状态码的描述
       >
       > 响应状态码的规则：
       >
       > - `1XX` 请求处理中
       > - `2XX` 请求成功
       > - `3XX` 表示请求重定向
       > - `4XX` 表示客户端错误
       > - `5XX` 表示服务器的错误

       > 响应头：响应头也是名值对结构
       >
       > - `Content-Type` 描述响应体的类型
       > - `Content-Length` 描述响应体的大小

       > 空行：分割响应头与响应体

       > 响应体：
       >
       > - 响应体就是服务器返回给客户端的内容
       > - `HTML、CSS、JavaScript、图片` 这些资源都会作为响应报文发送给客户端 
       >
       > ```html
       > <!DOCTYPE html>
       > <html lang="en">
       > 
       > <head>
       >     <meta charset="UTF-8">
       >     <meta name="viewport" content="width=device-width, initial-scale=1.0">
       >     <title>Document</title>
       > </head>
       > 
       > <body>
       >     <form method="post" action="./target.html">
       >         <input type="text" name="username">
       >         <input type="password" name="password">
       >         <button>提交</button>
       >     </form>
       > </body>
       > 
       > </html>
       > ```

       > `HTTP/1.1 200 OK Vary: Origin `
       >
       > `Access-Control-Allow-Credentials: true `
       >
       > `Accept-Ranges: bytes `
       >
       > `Cache-Control: public, max-age=0 Last-Modified: Thu, 07 Dec 2023 03:22:31 GMT ETag: W/"194-18c424c622c" `
       >
       > `Content-Type: text/html; charset=UTF-8 `
       >
       > `Content-Length: 1897 Date: Thu, 07 Dec 2023 03:41:46 GMT `
       >
       > `Connection: keep-alive Keep-Alive: timeout=5`

 - HTTP协议是一个无状态的协议，服务器无法区分请求是否发送自同一个客户端

   - `Cookie`

     - 是 HTTP 协议中用来解决无状态问的技术

     - `Cookie` 的本质就是一个头

       - 服务器以响应头的形式将 `Cookie` 发送给客户端，客户端收到后将其存储，在下次发送请求时传回，这样服务器就可以通过 `Cookie` 识别客户端

     - `cookie` 是有有效期的

       - 默认情况下 `cookie` 的有效期就是一次会话（session）
         - 会话就是一次打开到关闭浏览器的过程

     - `Node ExPress` 的使用

       ```bash
       ## 安装 cookie-parser
       yarn add cookie-parser
       ```

       ```javascript
       const express = require("express");
       
       // 引入 cookie-parser
       const cookieParser = require('cookie-parser')
       
       // 挂载 cookie-parser 中间件
       app.use(cookieParser())
       
       app.get('/set', (req, res)=>{
           res.cookie('name', 'huilu', {
               // expires: new Date()
               maxAge: 1000 * 60 * 60 * 24 * 30 // 毫秒为单位, 设置 cookie 过期时间
           })
           res.send("Cookie 设置成功.")
       })
       
       // 删除 cookie 只能通过重新设置覆盖的形式完成
       app.get('/deleate', (req, res)=>{
           // cookie 设置以后不可修改, 所以只能通过 重新设置的方式达到删除目的
           res.cookie('name', '', {
               maxAge: 0
           })
       
           res.send("Cookie 已删除.")
       })
       
       app.get('/get', (req, res)=>{
           console.log(req.cookies);
           res.send("Cookie 读取成功.")
       })
       ```

     - `Cookie`的不足

       - Cookie 是由服务器创建，浏览器保存
         - 每次浏览器访问服务器时都需要将 `Cookie`发回，导致不能在 `Cookie`中存储较大的数据
         - `Cookie`存储在客户端，容易被篡改
       - 注意：在使用 `Cookie`一定不会在`Cookie`存储敏感数据 
       - 为了解决`Cookie`的不足，我们希望可以将用户的数据统一存储在服务器中，每一个用户的数据都有一个对应的 ID 我们只需通过 `Cookie`将 ID 发送给浏览器，浏览器只需要将 ID 发回，即可读取到服务器中存储的数据
       - 这个技术称之为 `Session`

   - `Session`

     - `Session` 时服务器中一个对象，这个对象用来存储用户的数据

     - 每一个 `Session`对象都有唯一的ID，ID会通过 `Cookie` 的形式发送给客户端

     - 客户端每次访问时只需将存储 ID 的 `Cookie` 返回即可换取它在服务器中存储的数据

     - `Session`默认有效期是一次会话

     - 在 `Express`中通过 `express-session`组件实现 `Session`功能

       ```bash
       ## 安装 express-session
       yarn add express-session
       ```

       ```javascript
       const express = require("express");
       const session = require("express-session")
       
       const app = express();
       
       app.listen(3000, ()=>{
           console.log('server ok.');
       })
       
       // 挂载 session 中间件
       app.use(session({
           secret: "MiYao" // 加密密钥, 随便设置
       }))
       
       app.get('/set', (req, res)=>{
       
           req.session.username = "huilu"
           
           res.send("查看 Session.")
       })
       
       app.get('/get', (req, res)=>{
           const username = req.session.username
           console.log(username);
           res.send("读取 session.")
       })
       
       app.use((req, res) => {
           res.status(404).send("<h1>在光速飞行中迷失了方向~~</h1>")
       })
       
       ```

     - `Session`什么时候会失效？

       - 第一种： 浏览器的 `cookie` 没了
       - 第二中：服务器的 `session` 对象 没了

     - `express-session`默认是将 session 存储到内存中的，所以服务器一旦重启 session 会自动重启



