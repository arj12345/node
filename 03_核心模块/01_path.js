/*
    path
        - 表示的路径
        - 通过 path 可以用来获取各种路径
        - 要使用 path 先引入
        - 方法：
            path.resolve([...paths])
                - 用来生成一个绝对路径
                - 直接调用返回当前工作目录, 通过不同方式执行 node 工作目录可以变化
                - 传入相对，自动转为绝对路径
                - 传入两个参数，会对路径进行计算返回新路径
            
            fs（file System）
                - fs 用来帮助 node 操作磁盘中的文件
                - 文件操作就是所谓的 I/O 操作 
                - 需要引入
                    fs.readFile 读取文件
                    fs.mkdir 创建新文件
                    fs.rmdir 创建目录
                    fs.rm 删除文件
                    fs.rename 重命名
                    fs.copyFile 复制文件
*/
const path = require('node:path')
const fs = require("node:fs/promises")

// Promise fs 模块

// fs.readFile(path.resolve(__dirname, "./noted.txt")).then((buffer)=>{
//     console.log(buffer.toString())
// })
// .catch((reason)=>{
//     console.log("错了");
// })

// ;(async ()=>{
//     try {
//         const buffer = await fs.readFile(path.resolve(__dirname, "./noted.txt"))
//         console.log(buffer.toString())
//     }
//     catch(err) {
//         console.log("出错了")
//     }
// })()

// console.log(path.resolve('./hello.js'))

// 最终形态
// 以后使用路径时，尽量使用 path.resolve 计算生成
// const result = path.resolve(__dirname, './hello.js')
// console.log(result);

// 同步读取文件方法 readFileSync() 会阻塞后边的代码
// fs 模块读取文件，获取的是以 Buffer 对象的形式返回
// Buffer 是临时用来保存数据的缓冲区
/* const buf = fs.readFileSync(path.resolve(__dirname, "./noted.txt"))
console.log(buf.toString()) */

// fs.readFile(path.resolve(__dirname, "./noted.txt"), (err, buffer)   =>{
//     if(err) {
//         console.log(err)
//         return
//     }

//     console.log(buffer.toString())
// })
// console.log("老子出来了");


/* 
    mkdir（递归删除） 可以接受第二参数：
        接受一个对象，
            配置选项：recursive 默认值为 false
                - 设置为true, 自动创建不存在的上一级目录

    rmdir 也接受第二参数：递归删除
*/
fs.mkdir(path.resolve(__dirname, "./hello/abc"), { recursive: true })
    .then(r=>{
        console.log("成功.");
    })
    .catch(err=>{
        console.log("失败.");
    })

