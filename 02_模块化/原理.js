/*
    所有的 CommonJS 的模块都会被包装到一个函数中
    (function(exports, require, module, __filename, __dirname) {

    });
*/

// console.log(arguments);
console.log(__filename); // 当前模块的绝对路径
console.log(__dirname); // 当前模块所在目录