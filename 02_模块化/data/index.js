const data1 = require("./data1")
const data2 = require("./data2")
const data3 = require("./data3")

module.exports = {
    datas: {
        ...data1,
        ...data2,
        ...data3
    }
}