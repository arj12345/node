/*
    早期的网页中，没有一个实质性的规范
        我们实现模块化的方式，就是最原始的通过 script 标签来引入多个 js 文件
        问题：
            1. 无法选择要引入模块的那些内容
            2. 在复杂的模块场景下非常容易出错
            ....
        于是，我们就急需在 js 中引入一个模块化的解决方案
    
    在 node 中，默认支持的模块化规范叫做 CommonJS，
        在CommonJS中，一个 js 文件就是一个模块

    CommonJS 规范
        - 引入模块
            - 使用 require("模块路径") 函数来引入模块
            - 引入自定义模块时
                - 模块名要以 ./ 或 ../ 开头
                - 在CommonJs中，如果省略了 js 文件的扩展名
                    node，会自动为文件补全扩展名
                        ./m1.js 如果没有 js 它会寻找 ./m1.json
                        js --> json --> node（特殊）

            - 引入核心模块时
                - 直接些核心的名字即可
                - 也可以在核心模块前添加 node: 提高查找速度

        定义模块时，模块中的内容默认是不能被外部看到的
            可以通过 exports 来设置要想外部暴露的内容
        
        访问 exports 的方式有两种：
            exports
            module.exports
            - 当我们在其他模块中引入当前模块时，require函数返回的就是 exports
            - 可以将希望像外部暴露的内容设置为 exports 的属性
            - 可以通过 exports 一个一个的向外暴露
            - 也可以直接使用 module.exports 同时导出多个值

*/

// const m1 = require("./m1")

// console.log(m1)

// const m2 = require("./m2")

// console.log(m2)

// const path = require("path")
// const path = require("node:path")

// console.log(path);

// const m2 = require("./m2.cjs")

// console.log(m2);

// const data = require("./data")

// console.log(data);

console.log(module);

