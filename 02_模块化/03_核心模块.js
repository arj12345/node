// 核心模块，是 node 中自带的模块，可以在 node 中直接使用

// window 是浏览器的宿主对象 node 没有
// console.log(window);

// gloabl 是 node 中的全局对象, 类似于 window

// ES 标准下，全局对象的标准名应该是 globalThis
// console.log(globalThis);


/* 
    核心模块：
        process
            - 表示当前 node 的进程
            - 可以通过该对象何以获取进程的信息，或者对进程做各种操作
            - 如何使用：
                1. process 全局变量，可以全局使用
                2. 有哪些属性和方法：
                    process.exit()
                        - 结束当前进程, 终止 node
                    
                    process.nextTick(cllback[, ...argus])
                        - 将函数插入到 tick 队列
                        - tick 队列中的代码，会在下一次事件循环之前执行
                            会在微任务队列和宏任务队列 任务之前 执行

                调用栈 --> tick 队列 --> 微任务队列 --> 宏任务队列
*/


// console.log('你好')
// process.exit()
// console.log('尼玛我出不来了')


setTimeout(()=>{
    console.log(1); // 宏任务队列
})


queueMicrotask(()=>{
    console.log(2); // 微任务队列
})

process.nextTick(()=>{
    console.log(3); // tick 队列
})


console.log(4) // 调用栈