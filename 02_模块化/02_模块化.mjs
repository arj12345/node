/*
    默认情况下，node中的模块化标准时 CommonJS   
        想要使用ES的模块化，可以采用一下两种方案
            1. 使用 mjs 作为扩展名
            2. 修改 package.json 将模块化规范设置为 ES 模块
                当我们设置 "type": "module" 当前项目下所有的 js 文件都默认为 ES 模块化
*/
// console.log(module);

// 导入 m3 模块，es模块不能省略扩展名（官方标准）
// import { name, age, hobby } from "./m3.mjs"

// 通过 as 指定别名
// import { name as N, age, hobby } from "./m3.mjs"

// 开发时尽量避免 import * 情况
// import * as m3 from "./m3.mjs"

// 默认导出的内容，可以随意的命名
// import sum, { name, age, hobby } from "./m3.mjs";

// 通过 ES 模块化，导入的内容都是常量
// ES 模块都是运行在严格模式下的
// ES 在浏览器中同样支持，但是通常我们不会直接使用
//      通常结合打包工具使用
import { name, age, hobby } from "./m3.mjs";

hobby.push("OPX")
console.log(hobby);
