/*
    ES 模块化
*/

// 向外导出
export let name = "huilu"
export let age = 18
export let hobby = [
    "XXX",
    "OXO"
]

// 设置默认导出，一个模块中只有一个默认导出
export default function sum(a, b) {
    return a + b
}